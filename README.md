# Project Name
## Necessary tools

In order to develop this project you'll need the following tools:

- Git : [Download](https://git-scm.com/downloads)
- RPG Maker XP
- Ruby Host : [Download at PW Discord Server](https://discord.gg/0noB0gBDd91B8pMk)

## How to set everything up

At first, you need to install Git. If you're a Windows user, you'll need to create your SSH Key the following way :
- Open any folder
- Right click on the blank of the folder window
- Click on `Git Bash Here`
- Write `ssh-keygen`
- Press `Enter` without writing anything on all the request of `ssh-keygen`
- Go to the following folder `%USERPROFILE%/.ssh`
- In this folder you should see a `id_rsa.pub`
- Open `id_rsa.pub` with `Notepad` and copy the whole content (CTRL+A, CTRL+C)
- Go to your [Gitlab account in the SSH Key section](https://gitlab.com/profile/keys)
- Paste the content of `id_rsa.pub` in the `Key` field.
- Enter whatever you want in `Title`
- Enter nothing in `Expires at`
- Click on `Add Key`

With all those steps you're done setting up Git. Now you can set up your project !

- Go to [the project template](https://gitlab.com/pokemonsdk/project_template)
- Click on `Fork`
- Do all the steps that leads to a fork of project_template
- Go in your fork of `project_template` (You can find it [on this page](https://gitlab.com/dashboard/projects))
- On the left you should see a link called `Settings`, click on it
- Setup the information you want in `Naming, topics, avatar`
- Click on `Visibility, project features, permissions`
- Choose `Private` in `Project visibility` and save changes.
- Click on `Members` on the left
- In `GitLab member or Email address` put `@NuriYuri`
- In `Choose a role permission` put `Maintainer`
- Click on `Invite`

This will help NuriYuri to fix stuff in your project or other thing.

## How to clone the project

- Go on the project page, click on `Clone` and copy the URL that is `Clone with SSH`.
- Go in the folder where you want the clone, open `Git Bash` and write `git clone ` then paste the URL you copied and press enter. If everything is correct it should start cloning and generate a new folder.
- Now, copy all the project resources in the new project folder.
- Delete the folder called `pokemonsdk` in the new project folder.
- Delete the folder called `plugins` in the new project folder.

Once you copied everything you should open cmd.bat in the new folder and run the following commands :
- `git submodule add git@gitlab.com:pokemonsdk/pokemonsdk.git`
- `git submodule add git@gitlab.com:pokemonsdk/plugins.git`
- `git submodule update --init --recursive`
- `Game --util=convert`
- `git add -A`
- `git commit -m "Your Message"`
- `git push`

Now you should see the resources in the Git folder !

Btw remmeber that .rxdata, binaries and Audio files are not versionned. You have to distribute the Audio separatedly. You can [find the binaries here](https://gitlab.com/pokemonsdk/project/-/raw/master/extract_here_using_7zip.7z?inline=false) (extract the .7z file in your project folder). For the .rxdata files, the `Game --util=convert` command convert all the .rxdata files to .yml so you can see what's inside it and check the changes.

## How to push the project

When you want to update the project on gitlab, you have to push the modification. To do so perform the following command in cmd.bat :
- `Game --util=convert`
- `git add -A`
- `git commit -m "Your commit message"`
- `git push`

Don't forget to replace `Your commit message` by something that explain what you changed, like `Changed base HP of Caterpie`.

## How to pull the project

When you want to pull the modification of another developper you need to enter the following command in cmd.bat : 
- `git pull`
- `Game --util=restore`

The command restore convert all the .yml to .rxdata / .dat.

**Please don't forget to push before and solve the conflict if there's any !**

## How to update Pokémon SDK

Time to time you'll need to update PSDK to get all the new features, to do so you can use the updater (so you get the new images & csv) but since you're using the submodules you need to update them correctly. To do so enter the following command in cmd.bat :

- `cd pokemonsdk`
- `git reset --hard`
- `git pull`
- `cd ../plugins`
- `git reset --hard`
- `git pull`

## In case of problem

Ask Nuri Yuri and do some screen sharing. There's a whole part about branching and rebasing that is hard to explain so it's not written here.

Good luck!
